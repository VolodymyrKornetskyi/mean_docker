FROM node:latest

WORKDIR /usr/src/app

RUN npm i lerna -g --loglevel notice

COPY package.json .
COPY packages/backend ./packages/backend
COPY packages/frontend ./packages/frontend
COPY lerna.json .

RUN lerna bootstrap

CMD [ "npm", "run", "start-all"]
