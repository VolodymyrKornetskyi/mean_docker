import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(public http: HttpClient) {}

  title = 'cookbook';
  ngOnInit() {
    this.http.get('http://127.0.0.1:3000/recipes').subscribe(res => console.log(res));
  }
}
