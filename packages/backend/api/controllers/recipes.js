import mongoose from 'mongoose';
import Recipe from '../models/recipe';

export const getAll = (req, res) => Recipe.find()
    .exec()
    .then((docs) => {
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch((err) => {
        console.log(err);
        res.status(404).json({ error: err });
    });

export const getById = (req, res) => {
    res.send({});
};

export const create = async (req, res) => {
    try {
        console.log(req.body);
        const recipe = new Recipe({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            description: req.body.description,
            recipe: req.body.recipe,
            lastUpdateDate: new Date(),
            history: [],
        });
        await recipe.save();
        res.status(201).json({
            recipe,
        });
    } catch (e) {
        console.log(e);
        res.status(500).json({
            error: e,
        });
    }
};

export const deleteById = async (req, res) => {
    try {
        let status = 404;
        let message = 'Recipe with such id doesn\'t exist';
        const recipe = await Recipe.findOne({ _id: req.params.id });
        if (recipe) {
            await recipe.deleteOne({ _id: req.params.id });
            status = 200;
            message = `Successful deleted. ID: ${req.params.id}`;
        }

        await res.status(status).json({ message });
    } catch (e) {
        console.log(e);
        res.status(404).json({
            error: e,
        });
    }
};
