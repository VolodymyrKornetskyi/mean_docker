import express from 'express';
import { getAll, deleteById, create } from '../controllers/recipes';

const router = express.Router();

router.get('/', getAll);
router.post('/create', create);
router.delete('/:id', deleteById);

// router.post('/', (req, res, next) => {
//     const recipe = new Recipe({
//         _id: new mongoose.Types.ObjectId(),
//         name: req.body.name,
//         text: req.body.text
//     })
//     recipe
//         .save()
//         .then(result => {
//             console.log(result)
//         })
//         .catch(err => console.log(err));
//     res.status(201).json({
//         message: 'Handling POST requests to /recipes',
//         recipe: recipe
//     })
// })

// router.get('/:recipeId', (req, res, next) => {
//     const id = req.params.recipeId;
//     res.status(200).json({
//         message: id
//     })
// })

// router.patch('/:recipeId', (req, res, next) => {
//     const id = req.params.recipeId;
//     res.status(200).json({
//         message: "patched" + id
//     })
// })

// router.delete('/:recipeId', (req, res, next) => {
//     const id = req.params.recipeId;
//     res.status(200).json({
//         message: "deleted" + id
//     })
// })

export default router;
