import mongoose from 'mongoose';

const OldRecipeVariant = mongoose.Schema({
    name: String,
    recipe: String,
    description: String,
    invalidateDate: Date,
});

const RecipeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    recipe: String,
    description: String,
    lastUpdateDate: Date,
    history: [OldRecipeVariant],
});

export default mongoose.model('Recipe', RecipeSchema);
