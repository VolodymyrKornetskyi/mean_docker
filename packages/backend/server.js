import http from 'http';
import app from './app';

const port = 3000;

const server = http.createServer(app);

server.listen(
    port,
    (err) => console.log(err ? `Something bad happened. Error: ${err}` : `Server is listening on ${port}`),
);
